package com.vinaysshenoy.pockey

abstract class Proxy(val proxied: Storage) : Storage
