package com.vinaysshenoy.pockey

class PockeyStore(val storage: Storage) {

  val keys: Set<String>
    get() = storage.keys()

  fun contains(key: String) = storage.contains(key)

  fun remove(key: String) {
    storage.remove(key)
  }

  fun clear() {
    storage.clear()
  }

  fun putInt(key: String, value: Int?) {
    checkKeyNotBlank(key)
    storage.putInt(key, value)
  }

  fun putLong(key: String, value: Long?) {
    checkKeyNotBlank(key)
    storage.putLong(key, value)
  }

  fun putFloat(key: String, value: Float?) {
    checkKeyNotBlank(key)
    storage.putFloat(key, value)
  }

  fun putDouble(key: String, value: Double?) {
    checkKeyNotBlank(key)
    storage.putDouble(key, value)
  }

  fun putString(key: String, value: String?) {
    checkKeyNotBlank(key)
    storage.putString(key, value)
  }

  fun putBoolean(key: String, value: Boolean?) {
    checkKeyNotBlank(key)
    storage.putBoolean(key, value)
  }

  fun putBytes(key: String, value: ByteArray?) {
    checkKeyNotBlank(key)
    storage.putBytes(key, value)
  }


  @JvmOverloads
  fun getInt(key: String, defaultValueSupplier: () -> Int? = { null }): Int? {
    return try {
      storage.getInt(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getLong(key: String, defaultValueSupplier: () -> Long? = { null }): Long? {
    return try {
      storage.getLong(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getFloat(key: String, defaultValueSupplier: () -> Float? = { null }): Float? {
    return try {
      storage.getFloat(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getDouble(key: String, defaultValueSupplier: () -> Double? = { null }): Double? {
    return try {
      storage.getDouble(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getString(key: String, defaultValueSupplier: () -> String? = { null }): String? {
    return try {
      storage.getString(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getBoolean(key: String, defaultValueSupplier: () -> Boolean? = { null }): Boolean? {
    return try {
      storage.getBoolean(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  @JvmOverloads
  fun getBytes(key: String, defaultValueSupplier: () -> ByteArray? = { null }): ByteArray? {
    return try {
      storage.getBytes(key)
    } catch (keyNotPresentException: Storage.KeyNotPresentException) {
      defaultValueSupplier.invoke()
    }
  }

  private fun checkKeyNotBlank(key: String) {
    if (key.isBlank()) {
      throw BlankKeyException()
    }
  }

  class BlankKeyException : RuntimeException("Key must not be blank")
}
