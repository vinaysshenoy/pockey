package com.vinaysshenoy.pockey

import java.io.IOException
import java.lang.RuntimeException

interface Storage {

  @Throws(IOException::class)
  fun keys(): Set<String>

  @Throws(IOException::class)
  fun contains(key: String): Boolean

  @Throws(IOException::class)
  fun remove(key: String)

  @Throws(IOException::class)
  fun clear()

  @Throws(IOException::class)
  fun putInt(key: String, value: Int?)

  @Throws(IOException::class)
  fun getInt(key: String): Int?

  @Throws(IOException::class)
  fun putLong(key: String, value: Long?)

  @Throws(IOException::class)
  fun getLong(key: String): Long?

  @Throws(IOException::class)
  fun putFloat(key: String, value: Float?)

  @Throws(IOException::class)
  fun getFloat(key: String): Float?

  @Throws(IOException::class)
  fun putDouble(key: String, value: Double?)

  @Throws(IOException::class)
  fun getDouble(key: String): Double?

  @Throws(IOException::class)
  fun putString(key: String, value: String?)

  @Throws(IOException::class)
  fun getString(key: String): String?

  @Throws(IOException::class)
  fun putBoolean(key: String, value: Boolean?)

  @Throws(IOException::class)
  fun getBoolean(key: String): Boolean?

  @Throws(IOException::class)
  fun putBytes(key: String, value: ByteArray?)

  @Throws(IOException::class)
  fun getBytes(key: String): ByteArray?

  data class KeyNotPresentException(val key: String): RuntimeException("$key not present in store!")
}
