package com.vinaysshenoy.pockey

import java.io.IOException

open class SimpleProxy(proxied: Storage) : Proxy(proxied) {

  @Throws(IOException::class)
  override fun keys() = proxied.keys()

  @Throws(IOException::class)
  override fun contains(key: String): Boolean {
    return proxied.contains(key)
  }

  @Throws(IOException::class)
  override fun remove(key: String) {
    proxied.remove(key)
  }

  @Throws(IOException::class)
  override fun clear() {
    proxied.clear()
  }

  @Throws(IOException::class)
  override fun putInt(key: String, value: Int?) {
    proxied.putInt(key, value)
  }

  @Throws(IOException::class)
  override fun getInt(key: String) = proxied.getInt(key)

  @Throws(IOException::class)
  override fun putLong(key: String, value: Long?) {
    proxied.putLong(key, value)
  }

  @Throws(IOException::class)
  override fun getLong(key: String) = proxied.getLong(key)

  @Throws(IOException::class)
  override fun putFloat(key: String, value: Float?) {
    proxied.putFloat(key, value)
  }

  @Throws(IOException::class)
  override fun getFloat(key: String) = proxied.getFloat(key)

  @Throws(IOException::class)
  override fun putDouble(key: String, value: Double?) {
    proxied.putDouble(key, value)
  }

  @Throws(IOException::class)
  override fun getDouble(key: String) = proxied.getDouble(key)

  @Throws(IOException::class)
  override fun putString(key: String, value: String?) {
    proxied.putString(key, value)
  }

  @Throws(IOException::class)
  override fun getString(key: String) = proxied.getString(key)

  @Throws(IOException::class)
  override fun putBoolean(key: String, value: Boolean?) {
    proxied.putBoolean(key, value)
  }

  @Throws(IOException::class)
  override fun getBoolean(key: String) = proxied.getBoolean(key)

  @Throws(IOException::class)
  override fun putBytes(key: String, value: ByteArray?) {
    proxied.putBytes(key, value)
  }

  @Throws(IOException::class)
  override fun getBytes(key: String) = proxied.getBytes(key)
}
