package com.vinaysshenoy.pockey

import arguments
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.stubbing.OngoingStubbing
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isNull
import strikt.assertions.isTrue
import java.io.IOException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PockeyStoreTest {

  @ParameterizedTest(name = "keys in storage: {0}")
  @MethodSource("params for querying keys from storage")
  fun `it should query the keys from the storage`(expectedKeys: Set<String>) {
    val storage = mock<Storage>()
    whenever(storage.keys()).thenReturn(expectedKeys)
    val store = PockeyStore(storage)

    val keys = store.keys

    verify(storage).keys()
    expectThat(keys).isEqualTo(expectedKeys)
  }

  fun `params for querying keys from storage`(): List<Set<String>> {
    return listOf(
        emptySet(),
        setOf("key1"),
        setOf("key1", "key2", "key3")
    )
  }

  @ParameterizedTest(name = "key {0} is present in store: {1}")
  @MethodSource("params for querying if store contains key")
  fun `it should query the storage when checking if it contains a key`(key: String, expectedContains: Boolean) {
    val storage = mock<Storage>()
    whenever(storage.contains(key)).thenReturn(expectedContains)
    val store = PockeyStore(storage)

    val contains = store.contains(key)

    verify(storage).contains(key)
    expectThat(contains).isEqualTo(expectedContains)
  }

  fun `params for querying if store contains key`(): List<Arguments> {
    return listOf(
        arguments("key1", true),
        arguments("key2", false)
    )
  }


  @ParameterizedTest(name = "remove key: {0}")
  @ValueSource(strings = ["key1", "key2", "key3"])
  fun `it should remove from the storage when removing a key`(key: String) {
    val storage = mock<Storage>()
    val store = PockeyStore(storage)

    store.remove(key)

    verify(storage).remove(key)
  }

  @Test
  fun `it should clear the storage when clearing`() {
    val storage = mock<Storage>()
    val store = PockeyStore(storage)

    store.clear()

    verify(storage).clear()
  }

  @TestFactory
  fun `tests for putting values in the store`(): List<DynamicTest> {
    fun generateTest(
        displayName: String,
        execute: (PockeyStore) -> Unit,
        runVerifications: (Storage) -> Unit
    ): DynamicTest {
      return dynamicTest(displayName) {
        val storage = mock<Storage>()
        val store = PockeyStore(storage)

        execute(store)

        runVerifications(storage)
      }
    }

    return listOf(
        generateTest(
            displayName = "integers",
            execute = {
              it.putInt("key1", -1)
              it.putInt("key2", 1)
              it.putInt("key3", null)
            },
            runVerifications = {
              verify(it).putInt("key1", -1)
              verify(it).putInt("key2", 1)
              verify(it).putInt("key3", null)
            }
        ),
        generateTest(
            displayName = "longs",
            execute = {
              it.putLong("key1", -1L)
              it.putLong("key2", 1L)
              it.putLong("key3", null)
            },
            runVerifications = {
              verify(it).putLong("key1", -1L)
              verify(it).putLong("key2", 1L)
              verify(it).putLong("key3", null)
            }
        ),
        generateTest(
            displayName = "floats",
            execute = {
              it.putFloat("key1", -1.0F)
              it.putFloat("key2", 1.0F)
              it.putFloat("key3", null)
            },
            runVerifications = {
              verify(it).putFloat("key1", -1.0F)
              verify(it).putFloat("key2", 1.0F)
              verify(it).putFloat("key3", null)
            }
        ),
        generateTest(
            displayName = "doubles",
            execute = {
              it.putDouble("key1", -1.0)
              it.putDouble("key2", 1.0)
              it.putDouble("key3", null)
            },
            runVerifications = {
              verify(it).putDouble("key1", -1.0)
              verify(it).putDouble("key2", 1.0)
              verify(it).putDouble("key3", null)
            }
        ),
        generateTest(
            displayName = "strings",
            execute = {
              it.putString("key1", "")
              it.putString("key2", "value")
              it.putString("key3", null)
            },
            runVerifications = {
              verify(it).putString("key1", "")
              verify(it).putString("key2", "value")
              verify(it).putString("key3", null)
            }
        ),
        generateTest(
            displayName = "booleans",
            execute = {
              it.putBoolean("key1", true)
              it.putBoolean("key2", false)
              it.putBoolean("key3", null)
            },
            runVerifications = {
              verify(it).putBoolean("key1", true)
              verify(it).putBoolean("key2", false)
              verify(it).putBoolean("key3", null)
            }
        ),
        generateTest(
            displayName = "bytes",
            execute = {
              it.putBytes("key1", byteArrayOf(-1))
              it.putBytes("key2", byteArrayOf(0, -128, 127))
              it.putBytes("key3", null)
            },
            runVerifications = {
              verify(it).putBytes("key1", byteArrayOf(-1))
              verify(it).putBytes("key2", byteArrayOf(0, -128, 127))
              verify(it).putBytes("key3", null)
            }
        )
    )
  }

  @TestFactory
  fun `tests for throwing exceptions when a blank key is used to put values`(): List<DynamicTest> {
    val blankKeys = setOf("", "\t", " ", "  \t  ", "\r", "\n", "\r\n")

    fun generateTest(
        displayName: String,
        execute: (PockeyStore, String) -> Unit
    ): DynamicTest {
      return dynamicTest(displayName) {
        val storage = mock<Storage>()
        val store = PockeyStore(storage)

        blankKeys.forEachIndexed { index, blankKey ->
          assertThrows(PockeyStore.BlankKeyException::class.java, {
            execute(store, blankKey)
          }, "did not throw for key index #$index")
        }
      }
    }

    return listOf(
        generateTest(
            displayName = "integers",
            execute = { store, key -> store.putInt(key, 0) }
        ),
        generateTest(
            displayName = "longs",
            execute = { store, key -> store.putLong(key, 0L) }
        ),
        generateTest(
            displayName = "floats",
            execute = { store, key -> store.putFloat(key, 0F) }
        ),
        generateTest(
            displayName = "doubles",
            execute = { store, key -> store.putDouble(key, 0.0) }
        ),
        generateTest(
            displayName = "strings",
            execute = { store, key -> store.putString(key, "") }
        ),
        generateTest(
            displayName = "booleans",
            execute = { store, key -> store.putBoolean(key, true) }
        ),
        generateTest(
            displayName = "bytes",
            execute = { store, key -> store.putBytes(key, ByteArray(0)) }
        )
    )
  }

  @TestFactory
  fun `exceptions thrown by the storage must not be swallowed`(): List<DynamicTest> {
    fun generateTest(displayName: String, configureMock: (Storage) -> OngoingStubbing<*>, execute: (PockeyStore) -> Unit): DynamicTest {
      return dynamicTest(displayName) {
        val storage = mock<Storage>()
        configureMock(storage).thenThrow(IOException::class.java, RuntimeException::class.java)
        val store = PockeyStore(storage)

        assertThrows(IOException::class.java) { execute(store) }
        assertThrows(RuntimeException::class.java) { execute(store) }
      }
    }

    return listOf(
        generateTest(
            displayName = "getting keys",
            configureMock = { whenever(it.keys()) },
            execute = { it.keys }
        ),
        generateTest(
            displayName = "contains key",
            configureMock = { whenever(it.contains(any())) },
            execute = { it.contains("key") }
        ),
        generateTest(
            displayName = "removing key",
            configureMock = { whenever(it.remove(any())) },
            execute = { it.remove("key") }
        ),
        generateTest(
            displayName = "clearing storage",
            configureMock = { whenever(it.clear()) },
            execute = { it.clear() }
        ),
        generateTest(
            displayName = "put int",
            configureMock = { whenever(it.putInt(any(), any())) },
            execute = { it.putInt("key", 1) }
        ),
        generateTest(
            displayName = "get int",
            configureMock = { whenever(it.getInt(any())) },
            execute = { it.getInt("key") }
        ),
        generateTest(
            displayName = "put long",
            configureMock = { whenever(it.putLong(any(), any())) },
            execute = { it.putLong("key", 1L) }
        ),
        generateTest(
            displayName = "get long",
            configureMock = { whenever(it.getLong(any())) },
            execute = { it.getLong("key") }
        ),
        generateTest(
            displayName = "put float",
            configureMock = { whenever(it.putFloat(any(), any())) },
            execute = { it.putFloat("key", 1.0F) }
        ),
        generateTest(
            displayName = "get float",
            configureMock = { whenever(it.getFloat(any())) },
            execute = { it.getFloat("key") }
        ),
        generateTest(
            displayName = "put double",
            configureMock = { whenever(it.putDouble(any(), any())) },
            execute = { it.putDouble("key", 1.0) }
        ),
        generateTest(
            displayName = "get double",
            configureMock = { whenever(it.getDouble(any())) },
            execute = { it.getDouble("key") }
        ),
        generateTest(
            displayName = "put string",
            configureMock = { whenever(it.putString(any(), any())) },
            execute = { it.putString("key", "") }
        ),
        generateTest(
            displayName = "get string",
            configureMock = { whenever(it.getString(any())) },
            execute = { it.getString("key") }
        ),
        generateTest(
            displayName = "put boolean",
            configureMock = { whenever(it.putBoolean(any(), any())) },
            execute = { it.putBoolean("key", true) }
        ),
        generateTest(
            displayName = "get boolean",
            configureMock = { whenever(it.getBoolean(any())) },
            execute = { it.getBoolean("key") }
        ),
        generateTest(
            displayName = "put bytes",
            configureMock = { whenever(it.putBytes(any(), any())) },
            execute = { it.putBytes("key", byteArrayOf(1, 2, 3)) }
        ),
        generateTest(
            displayName = "get bytes",
            configureMock = { whenever(it.getBytes(any())) },
            execute = { it.getBytes("key") }
        )
    )
  }

  @TestFactory
  fun `tests for getting a value`(): List<DynamicTest> {
    fun generateTest(
        displayName: String,
        configureMock: (Storage) -> Unit,
        runVerifications: (PockeyStore) -> Unit
    ): DynamicTest {
      return dynamicTest(displayName) {
        val storage = mock<Storage>()
        configureMock(storage)
        val store = PockeyStore(storage)

        runVerifications(store)
      }
    }

    return listOf(
        generateTest(
            displayName = "integers",
            configureMock = {
              whenever(it.getInt("key1")).thenReturn(0, -1, -2)
              whenever(it.getInt("key2")).thenReturn(null)
              whenever(it.getInt("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getInt("key1")).isEqualTo(0)
                that(it.getInt("key1")).isEqualTo(-1)
                that(it.getInt("key1", defaultValueSupplier = { 5 })).isEqualTo(-2)
                that(it.getInt("key2")).isNull()
                that(it.getInt("key2", defaultValueSupplier = { 5 })).isNull()
                that(it.getInt("key3")).isNull()
                that(it.getInt("key3", defaultValueSupplier = { 5 })).isEqualTo(5)
                that(it.getInt("key3", defaultValueSupplier = { -5 })).isEqualTo(-5)
              }
            }
        ),
        generateTest(
            displayName = "longs",
            configureMock = {
              whenever(it.getLong("key1")).thenReturn(0L, -1L, -2L)
              whenever(it.getLong("key2")).thenReturn(null)
              whenever(it.getLong("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getLong("key1")).isEqualTo(0L)
                that(it.getLong("key1")).isEqualTo(-1L)
                that(it.getLong("key1", defaultValueSupplier = { 5L })).isEqualTo(-2L)
                that(it.getLong("key2")).isNull()
                that(it.getLong("key2", defaultValueSupplier = { 5L })).isNull()
                that(it.getLong("key3")).isNull()
                that(it.getLong("key3", defaultValueSupplier = { 5L })).isEqualTo(5L)
                that(it.getLong("key3", defaultValueSupplier = { -5L })).isEqualTo(-5L)
              }
            }
        ),
        generateTest(
            displayName = "floats",
            configureMock = {
              whenever(it.getFloat("key1")).thenReturn(0F, -1F, -2F)
              whenever(it.getFloat("key2")).thenReturn(null)
              whenever(it.getFloat("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getFloat("key1")).isEqualTo(0F)
                that(it.getFloat("key1")).isEqualTo(-1F)
                that(it.getFloat("key1", defaultValueSupplier = { -5F })).isEqualTo(-2F)
                that(it.getFloat("key2")).isNull()
                that(it.getFloat("key2", defaultValueSupplier = { -5F })).isNull()
                that(it.getFloat("key3")).isNull()
                that(it.getFloat("key3", defaultValueSupplier = { 5F })).isEqualTo(5F)
                that(it.getFloat("key3", defaultValueSupplier = { -1.5F })).isEqualTo(-1.5F)
              }
            }
        ),
        generateTest(
            displayName = "doubles",
            configureMock = {
              whenever(it.getDouble("key1")).thenReturn(0.0, -1.0, -2.0)
              whenever(it.getDouble("key2")).thenReturn(null)
              whenever(it.getDouble("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getDouble("key1")).isEqualTo(0.0)
                that(it.getDouble("key1")).isEqualTo(-1.0)
                that(it.getDouble("key1", defaultValueSupplier = { -5.0 })).isEqualTo(-2.0)
                that(it.getDouble("key2")).isNull()
                that(it.getDouble("key2", defaultValueSupplier = { -5.0 })).isNull()
                that(it.getDouble("key3")).isNull()
                that(it.getDouble("key3", defaultValueSupplier = { 5.0 })).isEqualTo(5.0)
                that(it.getDouble("key3", defaultValueSupplier = { -1.5 })).isEqualTo(-1.5)
              }
            }
        ),
        generateTest(
            displayName = "strings",
            configureMock = {
              whenever(it.getString("key1")).thenReturn("", "123", "asd")
              whenever(it.getString("key2")).thenReturn(null)
              whenever(it.getString("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getString("key1")).isEqualTo("")
                that(it.getString("key1")).isEqualTo("123")
                that(it.getString("key1", defaultValueSupplier = { "default" })).isEqualTo("asd")
                that(it.getString("key2")).isNull()
                that(it.getString("key2", defaultValueSupplier = { "default" })).isNull()
                that(it.getString("key3")).isNull()
                that(it.getString("key3", defaultValueSupplier = { "" })).isEqualTo("")
                that(it.getString("key3", defaultValueSupplier = { "default" })).isEqualTo("default")
              }
            }
        ),
        generateTest(
            displayName = "booleans",
            configureMock = {
              whenever(it.getBoolean("key1")).thenReturn(true, false)
              whenever(it.getBoolean("key2")).thenReturn(null)
              whenever(it.getBoolean("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getBoolean("key1")).isTrue()
                that(it.getBoolean("key1", defaultValueSupplier = { true })).isFalse()
                that(it.getBoolean("key2")).isNull()
                that(it.getBoolean("key2", defaultValueSupplier = { false })).isNull()
                that(it.getBoolean("key3")).isNull()
                that(it.getBoolean("key3", defaultValueSupplier = { true })).isTrue()
                that(it.getBoolean("key3", defaultValueSupplier = { false })).isFalse()
              }
            }
        ),
        generateTest(
            displayName = "bytes",
            configureMock = {
              whenever(it.getBytes("key1")).thenReturn(ByteArray(0), byteArrayOf(1, 2, 3), byteArrayOf(-128, 127, 0, 97, 45))
              whenever(it.getBytes("key2")).thenReturn(null)
              whenever(it.getBytes("key3")).thenThrow(Storage.KeyNotPresentException("key3"))
            },
            runVerifications = {
              expect {
                that(it.getBytes("key1")).isEqualTo(ByteArray(0))
                that(it.getBytes("key1")).isEqualTo(byteArrayOf(1, 2, 3))
                that(it.getBytes("key1", defaultValueSupplier = { ByteArray(0) })).isEqualTo(byteArrayOf(-128, 127, 0, 97, 45))
                that(it.getBytes("key2")).isNull()
                that(it.getBytes("key2", defaultValueSupplier = { ByteArray(0) })).isNull()
                that(it.getBytes("key3")).isNull()
                that(it.getBytes("key3", defaultValueSupplier = { ByteArray(0) })).isEqualTo(ByteArray(0))
                that(it.getBytes("key3", defaultValueSupplier = { byteArrayOf(1) })).isEqualTo(byteArrayOf(1))
              }
            }
        )
    )
  }
}
