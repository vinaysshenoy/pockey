package com.vinaysshenoy.pockey

import arguments
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.stubbing.OngoingStubbing
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.io.IOException

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SimpleProxyTest {

  @ParameterizedTest(name = "keys={0}")
  @MethodSource("params for requesting keys")
  fun `when requesting for the keys, it should get it from the proxied storage`(expectedKeys: Set<String>) {
    val proxied = mock<Storage>()
    whenever(proxied.keys()).thenReturn(expectedKeys)
    val proxy = SimpleProxy(proxied)

    val keys = proxy.keys()

    verify(proxied).keys()
    expectThat(keys).isEqualTo(expectedKeys)
  }

  fun `params for requesting keys`(): List<Set<String>> {
    return listOf(
        emptySet(),
        setOf("a"),
        setOf("a", "b"),
        setOf("a", "b", "c")
    )
  }

  @ParameterizedTest(name = "{0} is contained: {1}")
  @MethodSource("params for checking if contains key")
  fun `when requesting if it contains a key, it should get it from the proxied storage`(key: String, expectedContains: Boolean) {
    val proxied = mock<Storage>()
    whenever(proxied.contains(key)).thenReturn(expectedContains)
    val proxy = SimpleProxy(proxied)

    val contains = proxy.contains(key)

    verify(proxied).contains(key)
    expectThat(contains).isEqualTo(expectedContains)
  }

  fun `params for checking if contains key`(): List<Arguments> {
    return listOf(
        arguments("key1", true),
        arguments("key2", false)
    )
  }

  @ParameterizedTest(name = "key: {0}")
  @ValueSource(strings = ["key1", "key2"])
  fun `when requesting to remove a key, it should remove it from the proxied storage`(key: String) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.remove(key)

    verify(proxied).remove(key)
  }

  @Test
  fun `when requesting to clear storage, it should clear it from the proxied storage`() {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.clear()

    verify(proxied).clear()
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting integers")
  fun `when putting an integer, it should put it in the proxied storage`(key: String, value: Int?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putInt(key, value)

    verify(proxied).putInt(key, value)
  }

  fun `params for putting integers`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 0),
        arguments("key3", -50)
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting integers")
  fun `when getting an integer, it should get it from the proxied storage`(key: String, expectedValue: Int?) {
    val proxied = mock<Storage>()
    whenever(proxied.getInt(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getInt(key)

    verify(proxied).getInt(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting integers`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 10),
        arguments("key3", -10)
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting longs")
  fun `when putting a long, it should put it in the proxied storage`(key: String, value: Long?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putLong(key, value)

    verify(proxied).putLong(key, value)
  }

  fun `params for putting longs`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 0L),
        arguments("key3", -50L)
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting longs")
  fun `when getting a long, it should get it from the proxied storage`(key: String, expectedValue: Long?) {
    val proxied = mock<Storage>()
    whenever(proxied.getLong(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getLong(key)

    verify(proxied).getLong(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting longs`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 10L),
        arguments("key3", -10L)
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting floats")
  fun `when putting a float, it should put it in the proxied storage`(key: String, value: Float?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putFloat(key, value)

    verify(proxied).putFloat(key, value)
  }

  fun `params for putting floats`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 1F),
        arguments("key3", -50F)
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting floats")
  fun `when getting a float, it should get it from the proxied storage`(key: String, expectedValue: Float?) {
    val proxied = mock<Storage>()
    whenever(proxied.getFloat(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getFloat(key)

    verify(proxied).getFloat(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting floats`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 10F),
        arguments("key3", -10F)
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting doubles")
  fun `when putting a double, it should put it in the proxied storage`(key: String, value: Double?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putDouble(key, value)

    verify(proxied).putDouble(key, value)
  }

  fun `params for putting doubles`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 0.0),
        arguments("key3", -50.0)
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting doubles")
  fun `when getting a double, it should get it from the proxied storage`(key: String, expectedValue: Double?) {
    val proxied = mock<Storage>()
    whenever(proxied.getDouble(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getDouble(key)

    verify(proxied).getDouble(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting doubles`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", 10.0),
        arguments("key3", -10.0)
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting strings")
  fun `when putting a string, it should put it in the proxied storage`(key: String, value: String?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putString(key, value)

    verify(proxied).putString(key, value)
  }

  fun `params for putting strings`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", ""),
        arguments("key3", "value")
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting strings")
  fun `when getting a string, it should get it from the proxied storage`(key: String, expectedValue: String?) {
    val proxied = mock<Storage>()
    whenever(proxied.getString(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getString(key)

    verify(proxied).getString(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting strings`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", ""),
        arguments("key3", "value")
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting booleans")
  fun `when putting a boolean, it should put it in the proxied storage`(key: String, value: Boolean?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putBoolean(key, value)

    verify(proxied).putBoolean(key, value)
  }

  fun `params for putting booleans`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", true),
        arguments("key3", false)
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting booleans")
  fun `when getting a boolean, it should get it from the proxied storage`(key: String, expectedValue: Boolean?) {
    val proxied = mock<Storage>()
    whenever(proxied.getBoolean(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getBoolean(key)

    verify(proxied).getBoolean(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting booleans`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", true),
        arguments("key3", false)
    )
  }

  @ParameterizedTest(name = "Put {1} as {0}")
  @MethodSource("params for putting bytes")
  fun `when putting bytes, it should put it in the proxied storage`(key: String, value: ByteArray?) {
    val proxied = mock<Storage>()
    val proxy = SimpleProxy(proxied)

    proxy.putBytes(key, value)

    verify(proxied).putBytes(key, value)
  }

  fun `params for putting bytes`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", byteArrayOf(1)),
        arguments("key3", byteArrayOf(1, -128, 127, 96, -96))
    )
  }

  @ParameterizedTest(name = "Get {1} as {0}")
  @MethodSource("params for getting bytes")
  fun `when getting bytes, it should get it from the proxied storage`(key: String, expectedValue: ByteArray?) {
    val proxied = mock<Storage>()
    whenever(proxied.getBytes(key)).thenReturn(expectedValue)
    val proxy = SimpleProxy(proxied)

    val value = proxy.getBytes(key)

    verify(proxied).getBytes(key)
    expectThat(value).isEqualTo(expectedValue)
  }

  fun `params for getting bytes`(): List<Arguments> {
    return listOf(
        arguments("key1", null),
        arguments("key2", byteArrayOf(1)),
        arguments("key3", byteArrayOf(1, -128, 127, 96, -96))
    )
  }

  @TestFactory
  fun `tests for checking exceptions thrown by proxied storage are not swallowed`(): List<DynamicTest> {
    fun generateTest(displayName: String, configureMock: (Storage) -> OngoingStubbing<*>, execute: (SimpleProxy) -> Unit): DynamicTest {
      return dynamicTest(displayName) {
        val proxied = mock<Storage>()
        configureMock(proxied).thenThrow(IOException::class.java, RuntimeException::class.java)
        val proxy = SimpleProxy(proxied)

        assertThrows(IOException::class.java) { execute(proxy) }
        assertThrows(RuntimeException::class.java) { execute(proxy) }
      }
    }

    return listOf(
        generateTest(
            displayName = "getting keys",
            configureMock = { whenever(it.keys()) },
            execute = { it.keys() }
        ),
        generateTest(
            displayName = "contains key",
            configureMock = { whenever(it.contains(any())) },
            execute = { it.contains("key") }
        ),
        generateTest(
            displayName = "removing key",
            configureMock = { whenever(it.remove(any())) },
            execute = { it.remove("key") }
        ),
        generateTest(
            displayName = "clearing storage",
            configureMock = { whenever(it.clear()) },
            execute = { it.clear() }
        ),
        generateTest(
            displayName = "put int",
            configureMock = { whenever(it.putInt(any(), any())) },
            execute = { it.putInt("key", 1) }
        ),
        generateTest(
            displayName = "get int",
            configureMock = { whenever(it.getInt(any())) },
            execute = { it.getInt("key") }
        ),
        generateTest(
            displayName = "put long",
            configureMock = { whenever(it.putLong(any(), any())) },
            execute = { it.putLong("key", 1L) }
        ),
        generateTest(
            displayName = "get long",
            configureMock = { whenever(it.getLong(any())) },
            execute = { it.getLong("key") }
        ),
        generateTest(
            displayName = "put float",
            configureMock = { whenever(it.putFloat(any(), any())) },
            execute = { it.putFloat("key", 1.0F) }
        ),
        generateTest(
            displayName = "get float",
            configureMock = { whenever(it.getFloat(any())) },
            execute = { it.getFloat("key") }
        ),
        generateTest(
            displayName = "put double",
            configureMock = { whenever(it.putDouble(any(), any())) },
            execute = { it.putDouble("key", 1.0) }
        ),
        generateTest(
            displayName = "get double",
            configureMock = { whenever(it.getDouble(any())) },
            execute = { it.getDouble("key") }
        ),
        generateTest(
            displayName = "put string",
            configureMock = { whenever(it.putString(any(), any())) },
            execute = { it.putString("key", "") }
        ),
        generateTest(
            displayName = "get string",
            configureMock = { whenever(it.getString(any())) },
            execute = { it.getString("key") }
        ),
        generateTest(
            displayName = "put boolean",
            configureMock = { whenever(it.putBoolean(any(), any())) },
            execute = { it.putBoolean("key", true) }
        ),
        generateTest(
            displayName = "get boolean",
            configureMock = { whenever(it.getBoolean(any())) },
            execute = { it.getBoolean("key") }
        ),
        generateTest(
            displayName = "put bytes",
            configureMock = { whenever(it.putBytes(any(), any())) },
            execute = { it.putBytes("key", byteArrayOf(1, 2, 3)) }
        ),
        generateTest(
            displayName = "get bytes",
            configureMock = { whenever(it.getBytes(any())) },
            execute = { it.getBytes("key") }
        )
    )
  }
}
